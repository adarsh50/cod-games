import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { HYDRATE } from 'next-redux-wrapper'

// Define a service using a base URL and expected endpoints
export const shooterApi = createApi({
  reducerPath: 'shooterApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://free-to-play-games-database.p.rapidapi.com/api' }),
  tagTypes:["shooterApi"],
  extractRehydrationInfo(action, { reducerPath }) {
    if (action.type === HYDRATE) {
      return action.payload[reducerPath]
    }
  },
  endpoints: (builder) => ({
    getShooterByName: builder.query<object,void>({
      query: () => {
        return {
          url : "/games",
          method:"GET",
          params: {tag: '3d.mmorpg.fantasy.pvp', platform: 'pc'},
          headers: {
            'X-RapidAPI-Key': 'c3176a7de4msh2da1c84eb21b242p1cabb0jsn8549a1515f34',
            'X-RapidAPI-Host': 'free-to-play-games-database.p.rapidapi.com'
            }
            
        }
      }
      // provideTags:["shooterApi"],
    }),
    getShooterById:builder.query<object,{id:number}>({
      query: ({id}) => {
        return {
          url : `/game?id=${id}`,
          method:"GET",
          params: {tag: '3d.mmorpg.fantasy.pvp', platform: 'pc'},
          headers: {
            'X-RapidAPI-Key': 'c3176a7de4msh2da1c84eb21b242p1cabb0jsn8549a1515f34',
            'X-RapidAPI-Host': 'free-to-play-games-database.p.rapidapi.com'
            }
        }
      }
    })
  }),

})

export const { getShooterByName,getShooterById } = shooterApi.endpoints