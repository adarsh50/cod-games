import React, { ReactNode } from "react";

import { LayoutProps } from "../../../interfaces/interfaces";
import FooterContainer from "../footer/FooterContainer";
import HeaderContainer from "../header/HeaderContainer";

const Layout = ({ children }: LayoutProps) => {
  return (
    <>
      <HeaderContainer />
      <main>{children}</main>
      <FooterContainer />
    </>
  );
};

export default Layout;
