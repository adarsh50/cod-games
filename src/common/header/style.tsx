import React from "react";
import { styled, Box, List, Button } from "@mui/material";

const GamePageHeader = styled(Box, { name: "GamPageheader" })({
  backgroundColor: "#282828",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  "@media (min-width:0px) and (max-width:768px)": {
    justifyContent: "center",
    height: "60px",
    width: "auto",
    overflow: "auto",
  },
});

export const GamePageLogo = styled(Box)({
  margin: "6px 0 0 5px",
  "@media (min-width:0px) and (max-width:768px)": {
    img: {
      width: "30px",
      height: "20px",
    },
  },
});

export const HeaderList = styled(List)({
  color: "white",
  gap: "25px",
  display: "flex",
  "@media (min-width:0px) and (max-width:768px)": {
    gap: "10px",
  },
});

export const HeaderIcon = styled(Box)({
  display: "flex",
  gap: "20px",

  "@media (min-width:0px) and (max-width:768px)": {
    gap: "5px",
    paddingRight: "5px",
  },
});

export const HeaderLoginButton = styled(Button)({
  backgroundColor: "white",
  color: "black",
  "@media (min-width:0px) and (max-width:768px)": {
    width: "auto",
    height: "30px",
  },
});

export const HeaderSignUpButton = styled(Button)({
  backgroundColor: "white",
  color: "black",
  "@media (min-width:0px) and (max-width:768px)": {
    width: "auto",
    height: "30px",
  },
});

export default GamePageHeader;
