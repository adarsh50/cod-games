import { styled, Typography, Box, List } from "@mui/material";

const GameFooterPage = styled(Box)({
  marginTop: "10px",
  backgroundColor: "#282828",
  height: "auto",
  "@media (min-width:0px) and (max-width:768px)": {
    overflow: "auto",
    display: "flex",
    flexDirection: "column",
  },
});

export const GameFooterAbout = styled(Typography)({
  fontWeight: "700",
  color: "#fff",
  textTransform: "uppercase",
  fontSize: "24px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontWeight: "600",
    fontSize: "18px",
    marginTop: "10px",
  },
});

export const GameFooterAboutWrapper = styled(Typography)({
  fontSize: "16px",
  color: "#727489",
  margin: "20px 0 10px",
});

export const GameFooterExplore = styled(Typography)({
  fontWeight: "700",
  color: "#fff",
  textTransform: "uppercase",
  fontSize: "24px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontWeight: "600",
    fontSize: "18px",
    marginTop: "10px",
  },
});

export const GameFooterExploreList = styled(List)({
  color: "#727489",
  fontSize: "16px",
});

export const GameFooterOurGames = styled(Typography)({
  fontWeight: "700",
  color: "#fff",
  textTransform: "uppercase",
  fontSize: "24px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontWeight: "600",
    fontSize: "18px",
    marginTop: "10px",
  },
});

export const OurGamesList = styled(List)({
  color: "#727489",
  fontSize: "16px",
});

export const GameFooterContactUs = styled(Typography)({
  fontWeight: "700",
  color: "#fff",
  textTransform: "uppercase",
  fontSize: "24px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontWeight: "600",
    fontSize: "18px",
    marginTop: "10px",
  },
});

export const GameFooterAddressContainer = styled(Box)({
  display: "flex",
  marginTop: "15px",
});
export const GameFooterAddress = styled(Typography)({
  fontSize: "18px",
  fontWeight: "700",
  color: "#c4c4c4",
});

export const GameFooterAddressContain = styled(Typography)({
  color: "#727489",
  fontSize: "16px",
  marginLeft: "25px",
});

export const GameFooterEmailContainer = styled(Box)({
  display: "flex",
  marginTop: "15px",
});

export const GameFooterEmailAddress = styled(Typography)({
  fontSize: "18px",
  fontWeight: "700",
  color: "#c4c4c4",
});

export const GameFooterEmailWrapper = styled(Typography)({
  color: "#727489",
  fontSize: "16px",
  marginLeft: "25px",
});

export const GameFooterPhoneSection = styled(Box)({
  display: "flex",
  marginTop: "15px",
});

export const GameFooterPhoneContainer = styled(Typography)({
  fontSize: "18px",
  fontWeight: "700",
  color: "#c4c4c4",
});

export const GameFooterPhoneDescription = styled(Typography)({
  color: "#727489",
  fontSize: "16px",
  marginLeft: "25px",
});

export const GameFooterImageContainer = styled(Box)({
  marginTop: "30px",
});

export const GameFooterImageWrapper = styled(Box)({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
});

export const GameFooterIcon = styled(Box)({
  display: "flex",
  gap: "20px",
  justifyContent: "center",
  marginTop: "20px",
});

export const CopyRightsContainer = styled(Box)({
  display: "flex",
  justifyContent: "center",
  marginTop: "15px",
  paddingBottom: "26px",
});

export const CopyRightsWrapper = styled(Typography)({
  color: "white",
  fontSize: "16px",
});

export default GameFooterPage;
