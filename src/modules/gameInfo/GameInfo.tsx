import {
  Card,
  CardMedia,
  CardContent,
  Typography,
  Divider,
} from "@mui/material";

import { ShooterProps } from "../../../interfaces/interfaces";
import GameInfoContainer, {
  GameInfoAboutSection,
  GameInfoCard,
  GameInfoDescription,
  GameInfoTitle,
  GameInfoTitleWrapper,
} from "./style";

const GameInfo = ({ data }: ShooterProps) => {
  return (
    <GameInfoContainer>
      <div>
        <GameInfoCard>
          <CardMedia
            component="img"
            alt="green iguana"
            height="200px"
            image={data?.thumbnail}
          />
          <CardContent>
            <GameInfoTitle gutterBottom variant="h5">
              {data?.title}
            </GameInfoTitle>
            <Typography gutterBottom variant="h5" component="div">
              {data?.developer}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {data?.short_description}
            </Typography>
          </CardContent>
        </GameInfoCard>
      </div>
      <GameInfoAboutSection>
        <GameInfoTitleWrapper variant="h4">
          About {data?.title}
        </GameInfoTitleWrapper>
        <Divider />
        <GameInfoDescription variant="h3">
          {data?.description}
        </GameInfoDescription>
      </GameInfoAboutSection>
    </GameInfoContainer>
  );
};

export default GameInfo;
