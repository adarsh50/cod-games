import { ShooterProps } from "../../../interfaces/interfaces";
import GameInfo from "./GameInfo";

const GameInfoContainer = ({ data }: ShooterProps) => {
  return <GameInfo data={data} />;
};

export default GameInfoContainer;
