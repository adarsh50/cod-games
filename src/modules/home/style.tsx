import { styled, Box, Typography } from "@mui/material";

const HomeContainer = styled("div")({
  backgroundImage: 'url("https://wallpaperaccess.com/full/1409318.jpg")',
  height: "700px",
  objectFit: "cover",
  "@media (min-width:0px) and (max-width:768px)": {
    backgroundSize: "cover",
    width: "100%",
    backgroundPosition: "center center",
  },
});

export const HomeContainerWrapper = styled(Box)({
  position: "relative",
  display: "flex",
  justifyContent: "center",
  height: "100%",
  flexDirection: "column",
  marginLeft: "50px",
});

export const HomeActionGames = styled(Typography)({
  color: "#ff7a21",
  fontSize: "32px",
  fontWeight: "600",
  textTransform: "capitalize",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "30px",
    marginRight: "10px",
  },
});

export const HomeRealisticBattles = styled(Typography)({
  color: "#fff",
  fontSize: "80px",
  fontWeight: "700",
  textTransform: "capitalize",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "70px",
  },
});

export default HomeContainer;
