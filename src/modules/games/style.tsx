import { styled, Card } from "@mui/material";

const GameSection = styled("div")({
  padding: "20px",
});

export const CardList = styled(Card)({
  "&:hover": {
    transform: "translateY(10px)",
    boxShadow: "0px 5px 10px 0px rgb(60, 60, 60)",
  },
});

export default GameSection;
