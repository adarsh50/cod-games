import { props } from "../../../interfaces/interfaces";
import Games from "./Games";

const GameContainer = ({ data }: props) => {
  return <Games data={data} />;
};

export default GameContainer;
