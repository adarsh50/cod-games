import { useState } from "react";
import {
  Grid,
  CardMedia,
  Card,
  Box,
  CardContent,
  Typography,
  Pagination,
} from "@mui/material";
import Link from "next/link";

import { Game, props } from "../../../interfaces/interfaces";
import GameSection, { CardList } from "./style";

const Games = ({ data }: props) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(8);
  const handleChange = (event: any, newPage: number) => {
    setPage(newPage);
  };

  const length = data?.length / 12;

  return (
    <GameSection>
      <Grid container spacing={3}>
        {data
          ?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          .map((el, index) => (
            <Grid item xs={12} md={4} sm={6} xl={3} key={index}>
              <Link href={`/games/${el.id}`}>
                <CardList>
                  <CardMedia
                    component="img"
                    height="241px"
                    alt="shooterImg"
                    image={el.thumbnail}
                  />
                </CardList>
              </Link>

              <Box>
                <CardContent>
                  <Typography variant="h5">{el.title}</Typography>
                </CardContent>
              </Box>
            </Grid>
          ))}
      </Grid>
      <h1>Page Number : {page}</h1>
      <Pagination
        color="primary"
        count={Math.round(data?.length / 12)}
        page={page}
        onChange={handleChange}
        sx={{ rowsPerPage }}
      />
    </GameSection>
  );
};

export default Games;
