import { styled, Box, Typography } from "@mui/material";

const AboutContainer = styled(Box)({
  padding: "40px",
});

export const AboutContainerWrapper = styled(Typography)({
  display: "flex",
  justifyContent: "center",
  fontSize: "72px",
  fontWeight: "400",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "50px",
  },
});

export const AboutText = styled(Typography)({
  fontSize: "18px",
  "@media (min-width:0px) and (max-width:768px)": {
    fontSize: "16px",
    marginTop: "20px",
  },
});

export default AboutContainer;
