import { Box, Typography } from "@mui/material";

import BlogPostContainer, {
  BlogPostContain,
  BlogPostContainWrapper,
  BlogPostSection,
  BlogPostSectionWrapper,
  BlogPostsWrapper,
  BlogPostThirdSection,
} from "./style";

const Blog = () => {
  return (
    <BlogPostContainer>
      <Box>
        <BlogPostsWrapper variant="h1">Blog Posts</BlogPostsWrapper>
      </Box>
      <Box>
        <BlogPostSection variant="h4">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores
          cupiditate odio nesciunt cumque itaque, commodi et excepturi tempora
          eos rerum inventore dolorum quae consequuntur porro, debitis eius
          repellat nihil! Assumenda illum ullam accusamus porro aspernatur,
          molestias debitis quaerat sapiente officiis omnis consequuntur
          corrupti temporibus soluta magnam voluptate voluptas veniam,
          perspiciatis dignissimos maxime quia. Veritatis odio rerum, animi
          mollitia voluptatibus unde rem, omnis sunt minima totam quo dolores id
          vitae deleniti cupiditate iusto quas molestiae reiciendis, asperiores
          corrupti doloremque libero ad consequuntur. Quos architecto excepturi
          nihil optio quasi dolorem dicta odit, cum animi eum minus eaque
        </BlogPostSection>
      </Box>
      <BlogPostSectionWrapper>
        <BlogPostContain variant="h4">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores
          cupiditate odio nesciunt cumque itaque, commodi et excepturi tempora
          eos rerum inventore dolorum quae consequuntur porro, debitis eius
          repellat nihil! Assumenda illum ullam accusamus porro aspernatur,
          molestias debitis quaerat sapiente officiis omnis consequuntur
          corrupti temporibus soluta magnam voluptate voluptas veniam,
          perspiciatis dignissimos maxime quia. Veritatis odio rerum, animi
          mollitia voluptatibus unde rem, omnis sunt minima totam quo dolores id
          vitae deleniti cupiditate iusto quas molestiae reiciendis, asperiores
          corrupti doloremque libero ad consequuntur. Quos architecto excepturi
          nihil optio quasi dolorem dicta odit, cum animi eum minus eaque
          doloremque tempore, obcaecati eveniet corrupti nobis necessitatibus.
        </BlogPostContain>
      </BlogPostSectionWrapper>
      <BlogPostThirdSection>
        <BlogPostContainWrapper variant="h4">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores
          cupiditate odio nesciunt cumque itaque, commodi et excepturi tempora
          eos rerum inventore dolorum quae consequuntur porro, debitis eius
          repellat nihil! Assumenda illum ullam accusamus porro aspernatur,
          molestias debitis quaerat sapiente officiis omnis consequuntur
          corrupti temporibus soluta magnam voluptate voluptas veniam,
          perspiciatis dignissimos maxime quia. Veritatis odio rerum, animi
        </BlogPostContainWrapper>
      </BlogPostThirdSection>
    </BlogPostContainer>
  );
};

export default Blog;
