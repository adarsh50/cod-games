import { useRouter } from "next/router";
import { ShooterProps } from "../../interfaces/interfaces";
import Head from "next/head";

import { makeStore } from "../../store/configureStore";
import { getShooterById } from "../../store/api/shooter";
import GameInfoContainer from "../../src/modules/gameInfo/GameInfoContainer";
import { GetServerSideProps } from "next";

const ShooterGames = ({ data }: ShooterProps) => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <Head>
        <title>About Game</title>
        <meta name="description" content="next js about game" />
      </Head>
      <GameInfoContainer data={data} />
    </>
  );
};

export default ShooterGames;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { params } = context;
  const id = Number(params?.id);
  const store = makeStore();
  const data = await store.dispatch(getShooterById.initiate({ id }));

  // Pass data to the page via props
  return { props: data };
};
